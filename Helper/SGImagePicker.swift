//
//  SGImagePicker.swift
//  SysmindTest
//
//  Created by Vivek singh on 02/12/20.
//  Copyright © 2020 Vivek. All rights reserved.
//

import UIKit


//Keep strong reference


class SGImagePicker: NSObject {
    private override init() {}
    let imagePicker = UIImagePickerController()
    typealias CompletionBlock = (_ pickedImage: UIImage?)-> Void
    var completion: CompletionBlock?
    var enableEditing: Bool!
    
    enum ImageType {
        case camera
        case library
    }
    
    init(enableEditing: Bool) {
        super.init()
        imagePicker.delegate = self
        imagePicker.allowsEditing = enableEditing
        self.enableEditing = enableEditing
    }
    
    private func checkPlistKeys(isFor: ImageType) -> Bool {
        if isFor == .camera {
            if let camera = Bundle.main.object(forInfoDictionaryKey: "NSCameraUsageDescription") as? String {
                print(camera)
            } else {
                self.showAlert(title: "Camera", description: "NSCameraUsageDescription not found in Info.plist.")
                print("NSCameraUsageDescription not found in Info.plist.")
                return false
            }
        }else {
            if let library = Bundle.main.object(forInfoDictionaryKey: "NSPhotoLibraryUsageDescription") as? String {
                print(library)
            } else {
                self.showAlert(title: "Library", description: "NSPhotoLibraryUsageDescription not found in Info.plist.")
                print("NSPhotoLibraryUsageDescription not found in Info.plist.")
                return false
            }
        }
        return true
    }
    
    func getImage(from: ImageType, completion: @escaping CompletionBlock) {
        self.completion = completion
        if checkPlistKeys(isFor: from) {
//         guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
//            let sceneDelegate = windowScene.delegate as? SceneDelegate
//          else {
//            return
//          }
            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            let rootVC = appDelegate.window?.rootViewController
            if from == .camera {
                if !UIImagePickerController.isSourceTypeAvailable(.camera){
                    self.showAlert(title: "Camera", description: "Camera is not supprted")
                    return
                }
                imagePicker.sourceType = .camera
            } else {
                if !UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    self.showAlert(title: "Camera", description: "PhotoLibrary is not supprted")
                    return
                }
                imagePicker.sourceType = .photoLibrary
            }
            
            rootVC?.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    private func showAlert(title: String, description: String) {
//          guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
//                 let sceneDelegate = windowScene.delegate as? SceneDelegate
//               else {
//                 return
//               }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        let rootVC = appDelegate.window?.rootViewController
        
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        rootVC?.present(alert, animated: true, completion: nil)
    }
    
}

extension SGImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if enableEditing {
            if let image = info[.editedImage] as? UIImage{
                completion?(image)
            } else {
                completion?(nil)
            }
        } else {
            if let image = info[.originalImage] as? UIImage{
                completion?(image)
            } else {
                completion?(nil)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


//MARK: Extension UIViewController
extension UIViewController {

    
    func showOkAlert(_ msg: String) {
        let alert = UIAlertController(title:
            "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showOkAlertWithHandler(_ msg: String, handler: @escaping ()->Void){
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (type) -> Void in
            handler()
        }
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithActions(msg: String, titles: [String], handler:@escaping (_ clickedIndex: Int) -> Void) {
        if titles.last?.lowercased() == "cancel" {
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .actionSheet)
            for title in titles {
                if title.lowercased() == "cancel" {
                    let action  = UIAlertAction(title: title, style: .cancel, handler: { (alertAction) in
                        //fall the Call when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                } else {
                    let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                        //fall the Call when user clicked
                        let index = titles.index(of: alertAction.title!)
                        if index != nil {
                            handler(index!+1)
                        }
                        else {
                            handler(0)
                        }
                    })
                    alert.addAction(action)
                }
            }
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            present(alert, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
            
            for title in titles {
                
                let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                    //Call back fall when user clicked
                    let index = titles.index(of: alertAction.title!)
                    if index != nil {
                        handler(index!+1)
                    }
                    else {
                        handler(0)
                    }
                    
                })
                alert.addAction(action)
            }
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view //to set the source of your alert
                popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
            }
            present(alert, animated: true, completion: nil)
        }
    }
    
    func showOkCancelAlertWithAction(_ msg: String, handler:@escaping (_ isOkAction: Bool) -> Void) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let okAction =  UIAlertAction(title: "Yes", style: .default) { (action) -> Void in
            return handler(true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            return handler(false)
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}
