//
//  AddHeroesVC.swift
//  SysmindTest
//
//  Created by Vivek singh on 03/12/20.
//  Copyright © 2020 Vivek. All rights reserved.
//

import UIKit

class AddHeroesVC: UIViewController {

    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var descriptionTxtView: UITextView!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    let picker = SGImagePicker(enableEditing: true)
    var selectedImage : UIImage? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        customView.layer.cornerRadius = 10
        customView.layer.borderWidth = 1.0
        customView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTxtView.delegate = self
        descriptionTxtView.textColor = UIColor.lightGray
        descriptionTxtView.text = "Description"
        let clickOnImage = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
               self.profileImage.addGestureRecognizer(clickOnImage)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        let options = ["Camera","Library","Cancel"]
        self.showAlertWithActions(msg: "Please select an option.", titles: options) { (selected) in
            if selected == 1 {
                self.picker.getImage(from: .camera) { (image) in
                    
                    DispatchQueue.main.async {
                        self.selectedImage = image
                        self.profileImage.image = image
                    }
                    
                }
            } else if selected == 2{
                self.picker.getImage(from: .library) { (image) in
                    DispatchQueue.main.async {
                        self.selectedImage = image
                        self.profileImage.image = image
                    }
                    
                }
            }
        }
        
    }
    

    @IBAction func tapOnAddButon(_ sender: Any) {
        if isValidate() {
            self.showOkAlertWithHandler("Add successful!!") {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    func isValidate() -> Bool {
        if nameTxt.text! == "" {
            self.showOkAlertWithHandler("Your display name is required") {
                self.nameTxt.becomeFirstResponder()
            }
            return false
        }
        
        if descriptionTxtView.text! == "Description" {
            self.showOkAlertWithHandler("Your description is required") {
            }
            return false
        }
        return true
    }
    

}

extension AddHeroesVC:UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Description"{
            textView.textColor = UIColor.black
            textView.text = nil
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        debugPrint("updatedText:-\(newText)")
      
        return true
    }
    
}
