//
//  HeroesVC.swift
//  SysmindTest
//
//  Created by Vivek singh on 02/12/20.
//  Copyright © 2020 Vivek. All rights reserved.
//

import UIKit

class HeroesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var heroes: [Hero] = []
    var name: String?
    
    var loadingHeroes: Bool = false
    var currentPage: Int = 0
    var total = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Marvel Heroes"
        loadHeroes()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: "addMarval", sender: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func loadHeroes() {
        loadingHeroes = true
        if total == 0 {
            SwiftLoader.show(title: "Please wait...", animated: true)
        }
        MarvelAPI.loadHeroes(name: name, page: currentPage) { (info) in
            if let info = info {
                self.heroes += info.data.results
                self.total = info.data.total
                print(self.total)
                print(self.heroes.count)
                DispatchQueue.main.async {
                    SwiftLoader.hide()
                    self.loadingHeroes = false
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    
    
}

extension HeroesVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heroes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HeroTableViewCell
        
        let hero = heroes[indexPath.row]
        cell.prepareHero(with: hero)
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == heroes.count - 10 && !loadingHeroes && heroes.count != total {
            currentPage += 1
            loadHeroes()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete {
            heroes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
